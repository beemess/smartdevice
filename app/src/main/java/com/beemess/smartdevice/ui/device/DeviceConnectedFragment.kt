package com.beemess.smartdevice.ui.device

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.beemess.smartdevice.R
import com.beemess.smartdevice.constant.AppConst
import com.beemess.smartdevice.extension.requestLocationPermission
import com.beemess.smartdevice.extension.showError
import com.beemess.smartdevice.extension.showSnackbarShort
import com.beemess.smartdevice.interact.ISwitchScreen
import com.beemess.smartdevice.interact.OnViewEvent
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.exceptions.BleScanException
import kotlinx.android.synthetic.main.fragment_device_connected.view.*
import kotlinx.android.synthetic.main.fragment_device_scan.view.*

class DeviceConnectedFragment : Fragment(), OnViewEvent {

    private lateinit var deviceConnectedViewModel: DeviceConnectedViewModel

    private lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        deviceConnectedViewModel =
            ViewModelProviders.of(this).get(DeviceConnectedViewModel::class.java)
        deviceConnectedViewModel.setOnEventListener(this)
        rootView = inflater.inflate(R.layout.fragment_device_connected, container, false)
        val textView: TextView = rootView.findViewById(R.id.text_slideshow)
        deviceConnectedViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        arguments?.let {
            it.getString(AppConst.KEY_MAC_ADDRESS)?.let { macAddress ->
                Log.e("Device Address: ", macAddress)
                deviceConnectedViewModel.connect(macAddress)
            }

            it.getString(AppConst.KEY_DEVICE_NAME)?.let { deviceName ->
                activity?.title = deviceName
            }
        }
    }

    private fun initViews() {
        rootView.fbScanDevice.setOnClickListener {
            activity?.findNavController(R.id.nav_host_fragment)?.navigate(R.id.nav_device_scan)
        }
    }


    override fun onPause() {
        super.onPause()
        deviceConnectedViewModel.triggerDisconnect()
    }

    override fun onDestroy() {
        super.onDestroy()
        deviceConnectedViewModel.stateDisposable?.dispose()
    }

    /**
     * On View Event
     */
    override fun onError(error: BleScanException) {
        Log.e("Exception", error.localizedMessage ?: "")
        activity?.showError(error)
    }

    override fun onError(message: String) {
        activity?.showSnackbarShort(message)
    }

    override fun onError(throwable: Throwable) {
        Log.e("Exception", throwable.localizedMessage ?: "")
        activity?.showSnackbarShort("Error: $throwable")
    }

    override fun toggleProgressBar(show: Boolean) {
    }
}
