package com.beemess.smartdevice.ui.scanning

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.beemess.smartdevice.R
import com.beemess.smartdevice.constant.AppConst
import com.beemess.smartdevice.extension.requestLocationPermission
import com.beemess.smartdevice.extension.showError
import com.beemess.smartdevice.extension.showSnackbarShort
import com.beemess.smartdevice.interact.OnItemClickListener
import com.beemess.smartdevice.interact.OnPermissonEvent
import com.beemess.smartdevice.interact.OnViewEvent
import com.beemess.smartdevice.ui.device.DeviceConnectedFragmentDirections
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.exceptions.BleScanException
import com.polidea.rxandroidble2.scan.ScanResult
import kotlinx.android.synthetic.main.fragment_device_scan.view.*

class DeviceScanningFragment : Fragment(), OnScanResultChanged, OnViewEvent, OnItemClickListener,
    OnPermissonEvent {

    private lateinit var deviceScanningViewModel: DeviceScanningViewModel
    private lateinit var listDeviceAdapter: ListDeviceAdapter
    private lateinit var rootView: View

    private val isScanning: Boolean
        get() = deviceScanningViewModel.scanDisposable != null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        deviceScanningViewModel =
            ViewModelProviders.of(this).get(DeviceScanningViewModel::class.java)
        deviceScanningViewModel.setOnScanChangedListener(this)
        deviceScanningViewModel.setOnViewEvent(this)
        deviceScanningViewModel.setOnPermissionEvent(this)
        rootView = inflater.inflate(R.layout.fragment_device_scan, container, false)
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
        configureListDevice()
        checkBluetoothStatus()
    }

    private fun initViews() {
        rootView.btnScanDevice.setOnClickListener {
            if (isScanning) {
                deviceScanningViewModel.dispose()
            } else {
                rootView.pbListDevice.visibility = View.VISIBLE
                deviceScanningViewModel.scanBleDevices()
            }
        }
    }

    private fun configureListDevice() {
        listDeviceAdapter = ListDeviceAdapter(deviceScanningViewModel.listDevices, this)
        rootView.rcListDevices.adapter = listDeviceAdapter
    }

    private fun checkBluetoothStatus() {
        if (!deviceScanningViewModel.isBluetoothSupported()) {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Not Compatible")
            builder.setMessage("Your phone does not support Bluetooth")
            builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, _ ->
                dialog.dismiss()
            })
            builder.setIcon(R.drawable.ic_error)
            builder.show()
        } else if (!deviceScanningViewModel.isBluetoothEnabled()) {
            deviceScanningViewModel.startBluetoothManagerActivity(this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AppConst.REQUEST_ENABLE_BT) {
            Toast.makeText(context, "Bluetooth enabled", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onItemChanged(index: Int) {
        listDeviceAdapter.notifyItemChanged(index)
    }

    override fun onDataSetChanged() {
        listDeviceAdapter.notifyDataSetChanged()
    }

    override fun onItemClick(result: ScanResult) {
        Log.e("OnItemClick", "" + result.bleDevice.macAddress)
        //deviceScanningViewModel.connect(result)

        val deviceName = result.bleDevice.name ?: "Device"
        val action = DeviceScanningFragmentDirections.actionNavDeviceScanToNavDeviceConnect(
            result.bleDevice.macAddress,
            deviceName
        )
        activity?.findNavController(R.id.nav_host_fragment)?.navigate(action)
    }

    /**
     * View Event listener
     */
    override fun onError(error: BleScanException) {
        activity?.showError(error)
    }

    override fun onError(message: String) {
        activity?.showSnackbarShort(message)
    }

    override fun onError(throwable: Throwable) {
        activity?.showSnackbarShort("Error: $throwable")
    }

    override fun onRequestLocationPermission(client: RxBleClient) {
        activity?.requestLocationPermission(client)
    }

    override fun toggleProgressBar(show: Boolean) {
        rootView.pbListDevice.visibility = if (show) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}

interface OnScanResultChanged {
    fun onItemChanged(index: Int)
    fun onDataSetChanged()
}

