package com.beemess.smartdevice.ui.scanning

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.beemess.smartdevice.R
import com.beemess.smartdevice.interact.OnItemClickListener
import com.polidea.rxandroidble2.scan.ScanResult
import kotlinx.android.synthetic.main.list_item_smart_device.view.*

internal class ListDeviceAdapter(
    private val listData: MutableList<ScanResult>,
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ListDeviceAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_smart_device, parent, false)
            .let {
                ViewHolder(it)
            }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = listData[position]
        with(result) {
            holder.bindViewHolder(this)
            holder.itemView.setOnClickListener {
                onItemClickListener.onItemClick(result)
            }
        }
    }


    fun clearData() {
        listData.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindViewHolder(result: ScanResult) {
            itemView.tvDeviceName.text = result.bleDevice.name
            itemView.tvDeviceMacAdd.text = result.bleDevice.macAddress
        }
    }
}