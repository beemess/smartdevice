package com.beemess.smartdevice.ui.device

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beemess.smartdevice.application.SmartDeviceApplication
import com.beemess.smartdevice.extension.isConnected
import com.beemess.smartdevice.extension.toHex
import com.beemess.smartdevice.interact.OnViewEvent
import com.jakewharton.rx.ReplayingShare
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import com.polidea.rxandroidble2.scan.ScanResult
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.DisposableHelper.dispose
import io.reactivex.internal.operators.flowable.FlowableBlockingSubscribe.subscribe
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.*

class DeviceConnectedViewModel : ViewModel() {

    private val rxBleClient = SmartDeviceApplication.rxBleClient
    private val _text = MutableLiveData<String>().apply {
        value = "This is slideshow Fragment"
    }
    val text: LiveData<String> = _text

    private lateinit var bleDevice: RxBleDevice
    private var connectionDisposable: Disposable? = null
    private var connectionDisposableRead = CompositeDisposable()
    var stateDisposable: Disposable? = null
    private lateinit var onViewEvent: OnViewEvent
    private val characteristicUuid = UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26a8")

    private lateinit var connectionObservable: Observable<RxBleConnection>
    private val disconnectTriggerSubject = PublishSubject.create<Unit>()


    fun setOnEventListener(listener: OnViewEvent) {
        onViewEvent = listener
    }

    private fun prepareConnectionObservable(): Observable<RxBleConnection> =
        bleDevice
            .establishConnection(false)
            .takeUntil(disconnectTriggerSubject)
            .compose(ReplayingShare.instance())

    private fun triggerDisconnectRead() = disconnectTriggerSubject.onNext(Unit)

    /*************************************************************
     * For Device Connection
     *************************************************************/

    fun connect(macAddress: String) {
        bleDevice = rxBleClient.getBleDevice(macAddress)
        bleDevice.observeConnectionStateChanges()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { onConnectionStateChanged(it) }
            .let { stateDisposable = it }
        connectToDevice()
    }

    private fun connectToDevice() {
        connectionObservable = prepareConnectionObservable()
        if (bleDevice.isConnected) {
            triggerDisconnect()
        } else {
            connectionObservable
                .flatMapSingle { it.discoverServices() }
                .flatMapSingle { it.getCharacteristic(characteristicUuid) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { characteristic ->
                        _text.value = characteristic.toString()
                        Log.i(javaClass.simpleName, "Hey, connection has been established!")
                        readData()
                    },
                    { onConnectionFailure(it) }
                )
                .let { connectionDisposableRead.add(it) }
        }
    }

    private fun onConnectionFailure(throwable: Throwable) {
        onViewEvent.onError(throwable)
    }

    private fun onConnectionReceived() {
        onViewEvent.onError("Connection received")
        _text.value = "Device Connected"
        read()
    }

    fun triggerDisconnect() = connectionDisposable?.dispose()

    private fun onConnectionStateChanged(newState: RxBleConnection.RxBleConnectionState) {
        onViewEvent.onError(newState.toString())
    }

    private fun dispose() {
        connectionDisposable?.dispose()
        stateDisposable?.dispose()
        connectionDisposableRead.dispose()
    }

    /**
     * read data˙
     */
    private fun readData() {
        connectionObservable
            .firstOrError()
            .flatMap { it.readCharacteristic(characteristicUuid) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ bytes ->
                Log.e("REadData", String(bytes))
                _text.value = String(bytes)
            }, { onReadFailure(it) })
            .let { connectionDisposableRead.add(it) }
    }

    private fun read() {
        connectionObservable = prepareConnectionObservable()
        bleDevice.establishConnection(false)
            .flatMapSingle { it.readCharacteristic(UUID.randomUUID()) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ characteristicValue ->
                _text.value = String(characteristicValue)
            }, { throwable ->
                onViewEvent.onError(throwable)
            }
            ).let { connectionDisposableRead.add(it) }
    }

    private fun onReadFailure(throwable: Throwable) {
        onViewEvent.onError(throwable)
    }
}