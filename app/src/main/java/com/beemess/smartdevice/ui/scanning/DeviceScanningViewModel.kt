package com.beemess.smartdevice.ui.scanning

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beemess.smartdevice.application.SmartDeviceApplication
import com.beemess.smartdevice.constant.AppConst
import com.beemess.smartdevice.interact.OnPermissonEvent
import com.beemess.smartdevice.interact.OnViewEvent
import com.polidea.rxandroidble2.exceptions.BleScanException
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class DeviceScanningViewModel : ViewModel() {

    private val rxBleClient = SmartDeviceApplication.rxBleClient
    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    var scanDisposable: Disposable? = null
    var listDevices = mutableListOf<ScanResult>()
    private lateinit var onScanChangedListener: OnScanResultChanged
    private lateinit var onViewEvent: OnViewEvent
    private lateinit var onPermissionEvent: OnPermissonEvent

    /************************************************
     * For Device Scanning
     ************************************************/
    fun scanBleDevices() {
        if (rxBleClient.isScanRuntimePermissionGranted) {
            scan()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doFinally { dispose() }
                .subscribe(this::onScanSuccess, this::onScanFailure)
                .let { scanDisposable = it }
        } else {
            onPermissionEvent.onRequestLocationPermission(rxBleClient)
        }
    }

    private fun scan(): Observable<ScanResult> {
        val scanSetting = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
            .build()
        val filter = ScanFilter.Builder()
            .setDeviceName("BeeGreen")
            .build()
        return rxBleClient.scanBleDevices(scanSetting, filter)
    }

    fun setOnScanChangedListener(listener: OnScanResultChanged) {
        this.onScanChangedListener = listener
    }

    fun setOnPermissionEvent(listener: OnPermissonEvent) {
        this.onPermissionEvent = listener
    }

    fun setOnViewEvent(listener: OnViewEvent) {
        this.onViewEvent = listener
    }

    private fun addScanResult(bleResult: ScanResult) {
        // Not the best way to ensure distinct devices, just for the sake of the demo.
        listDevices.withIndex()
            .firstOrNull { it.value.bleDevice == bleResult.bleDevice }
            ?.let {
                // device already in data list => update
                listDevices[it.index] = bleResult
                onScanChangedListener.onItemChanged(it.index)
            }
            ?: run {
                // new device => add to data list
                with(listDevices) {
                    add(bleResult)
                    sortBy { it.bleDevice.macAddress }
                }
                onScanChangedListener.onDataSetChanged()
            }
    }

    private fun onScanSuccess(result: ScanResult) {
        Log.e("Device Scanned", result.bleDevice.name ?: "NULL")
        onViewEvent.toggleProgressBar(false)
        addScanResult(result)
    }

    private fun onScanFailure(throwable: Throwable) {
        onViewEvent.toggleProgressBar(false)
        if (throwable is BleScanException)
            onViewEvent.onError(throwable)
    }

    fun dispose() {
        scanDisposable = null
        listDevices.clear()
        onScanChangedListener.onDataSetChanged()
    }

    fun isBluetoothEnabled(): Boolean {
        return BluetoothAdapter.getDefaultAdapter().isEnabled
    }

    //**************************************************************

    fun isBluetoothSupported(): Boolean {
        return BluetoothAdapter.getDefaultAdapter() != null
    }

    fun startBluetoothManagerActivity(frg: Fragment) {
        val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        frg.startActivityForResult(intent, AppConst.REQUEST_ENABLE_BT)
    }
}