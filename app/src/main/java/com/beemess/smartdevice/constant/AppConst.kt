package com.beemess.smartdevice.constant

class AppConst {
    companion object {
        const val REQUEST_ENABLE_BT = 1

        //Define id of screen to translate
        const val FRG_DEVICE_SCANNING = 1
        const val FRG_DEVICE_CONNECTED = 2

        //Define key of bundle
        const val KEY_MAC_ADDRESS = "key_mac_address"
        const val KEY_DEVICE_NAME = "key_device_name"
    }
}