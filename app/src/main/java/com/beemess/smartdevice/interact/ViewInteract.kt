package com.beemess.smartdevice.interact

import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.exceptions.BleScanException
import com.polidea.rxandroidble2.scan.ScanResult

interface OnItemClickListener {
    fun onItemClick(result: ScanResult)
}

interface OnViewEvent {
    fun onError(error: BleScanException)
    fun toggleProgressBar(show: Boolean)
    fun onError(throwable: Throwable)
    fun onError(message: String)
}

interface OnPermissonEvent {
    fun onRequestLocationPermission(client: RxBleClient)
}