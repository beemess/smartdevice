package com.beemess.smartdevice.interact

import android.os.Bundle

interface ISwitchScreen {
    fun translateTo(screenId: Int, bundle: Bundle?)
}